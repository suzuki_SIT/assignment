import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JButton doriaButton;
    private JButton pastaButton;
    private JButton penneButton;
    private JTextPane receivedInfo;
    private JButton pizzaButton;
    private JButton drinkButton;
    private JButton tiramisuButton;
    private JLabel Total;
    private JLabel sum;
    private JButton checkoutButton;

    int subtotal = 0; //subtotal
    int orderflag = 1;
    int setflag = 1;  //1:not ordered drink

    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+ food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0) {
            JOptionPane.showMessageDialog(null,
                    "Order for "+ food +" received.");

            if(orderflag == 1) {
                receivedInfo.setText(null);
                receivedInfo.setText(food + "   " + price + " yen");
            }
            else {
                String currentText = receivedInfo.getText();
                receivedInfo.setText(currentText + "\n" + food + "   " + price + " yen");
            }

            if(food.equals("drink")) {
                setflag = 0;
            }

            subtotal += price;
            sum.setText(subtotal+" yen");
            orderflag = 0;
        }
    }

    public SimpleFoodOrderingMachine() {
        doriaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Doria",300);
            }
        });
        doriaButton.setIcon(new ImageIcon(this.getClass().getResource("./media/doria.jpg")));


        pastaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pasta",400);
            }
        });
        pastaButton.setIcon(new ImageIcon(this.getClass().getResource("./media/pasta.jpg")));


        penneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Penne",450);
            }
        });
        penneButton.setIcon(new ImageIcon(this.getClass().getResource("./media/penne.jpg")));


        pizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pizza",350);
            }
        });
        pizzaButton.setIcon(new ImageIcon(this.getClass().getResource("./media/pizza.jpg")));


        tiramisuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tiramisu",200);
            }
        });
        tiramisuButton.setIcon(new ImageIcon(this.getClass().getResource("./media/tiramisu.jpg")));


        drinkButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("drink", 320);

            }
        });
        drinkButton.setIcon(new ImageIcon(this.getClass().getResource("./media/drink.jpg")));


        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Do you really check out?",
                        "Check Out Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    if(subtotal > 320 && setflag == 0) {
                        JOptionPane.showMessageDialog(null,
                                "Thank you!\n" +
                                        "Because the set price will be applied," +
                                        "the total price is ...\n"
                                        + subtotal + "- 220 : " + (subtotal-220) + " yen");
                    }
                    else {
                        JOptionPane.showMessageDialog(null,
                                "Thank you!\nThe total price is " + subtotal + " yen");
                    }

                    //reset various flags,text
                    subtotal = 0;
                    orderflag = 1;
                    setflag = 1;
                    receivedInfo.setText("No order received");
                    sum.setText("0 yen");
                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
